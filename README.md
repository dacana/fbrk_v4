## Thông tin

**Deployment**

- Docker Container
- Docker Compose
- Mỗi Service (Container sẽ có môi trường khác nhau : LEMP, LAMP, NODE...)
- URL: IP Docker Machine (localhost/192.168.99.100...)

*localhost: để test các service cũng như thêm các service khác để demo trước khi tạo trên Production*

**Localhost**

nginx-gateway

- API Gateway, dùng Nginx/Debian
- ports:
    - 80:80
    - 443:443

distribution-be : 

- LEMP

- ULR: 192.168.99.100:20080 [Web UI]

- ports:
    - "20080:80"

distribution-auth-service

- LEMP

- ULR: 192.168.99.100:20180 [Web UI]

- ports:
    - "20180:80"

distribution-fe

- LEMP

- ULR: 192.168.99.100:20280 [Web UI]

- ports:
    - "20280:80"

rabbitmq

- ULR: 192.168.99.100:15672 [Web UI : user:password]

- ports:
    - "5672:5672"
    - "15672:15672"
    - "15674:15674"

rethinkdb

- ULR: 192.168.99.100:8080 [Web UI]

- ports:
    - "28015:28015"
    - "29015:29015"
    - "8080:8080"

**Dev**

N/A

**Production**

N/A

---

## Hướng dẫn

- Tài liệu [chi tiết](https://bitbucket.org/dacana/fbrk_v4/wiki/) [Updating]

**Cài đặt Docker, Docker Compose**

- Tài liệu tham khảo [docker compose](https://docs.docker.com/compose/)

**Clone Projects**

- *Dùng SourceTree*

- *Dùng Command*

    - `git clone git@bitbucket.org:dacana/fbrk_v4.git`

**Run Project**

- *Chạy command line sau*

    - `docker-compose up --build`

**Test Project**

- Vào các url:port như phần **Thông tin** để test.

---

## Release Notes

- 2019-10-03: Version 1.00, Tạo các service cơ bản.

---